#!/bin/bash

sam package --template-file template.yaml  --output-template-file packaged.yaml --s3-bucket ceh-dynamicip-cloudfront
sam deploy --template-file packaged.yaml --stack-name DynamicIP --capabilities CAPABILITY_IAM