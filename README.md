# DynamicIP

This application will take an old IP address, and update it's instance to the new ip address.  Designed to be called by a server when it detects an IP change.

```bash
.
├── README.md                   <-- This instructions file
├── DynamicIP                   <-- Source code for a lambda function
│   ├── index.js                  <-- Lambda function code
│   ├── package.json            <-- NodeJS dependencies
└── template.yaml               <-- SAM template
```

## Local Dev Requirements

* AWS CLI already configured with at least PowerUser permission
* [NodeJS 8.10+ installed](https://nodejs.org/en/download/)
* [Docker installed](https://www.docker.com/community-edition) for local testing

## Deployment Requirements

* Server set up to detect IP Changes
* AWS CLI configured on above mentioned server

## Setup process

### Installing dependencies

In this example we use `npm` but you can use `yarn` if you prefer to manage NodeJS dependencies:

```bash
cd DynamicIP
npm install
cd ../
```

### Local development

**Invoking function locally through local API Gateway**

```bash
sam local invoke --event event.json 
```

If the previous command ran successfully you should now be able to browse to AWS Route 53 and see all IP addresses changed from 1.1.1.1 to 2.2.2.2, or whatever is configured within the event.json file


## Packaging and deployment

AWS Lambda NodeJS runtime requires a flat folder with all dependencies including the application. SAM will use `CodeUri` property to know where to look up for both application and dependencies:

```yaml
...
    DynamicIPFunction:
        Type: AWS::Serverless::Function
        Properties:
            CodeUri: DynamicIP/
            ...
```

Firstly, we need a `S3 bucket` where we can upload our Lambda functions packaged as ZIP before we deploy anything - If you don't have a S3 bucket to store code artifacts then this is a good time to create one:

```bash
aws s3 mb s3://BUCKET_NAME
```

Next, run the following command to package our Lambda function to S3:

```bash
sam package \
    --template-file template.yaml \
    --output-template-file packaged.yaml \
    --s3-bucket REPLACE_THIS_WITH_YOUR_S3_BUCKET_NAME
```

Or run the command 

```bash
./package.sh REPLACE_THIS_WITH_YOUR_S3_BUCKET_NAME
```

Next, the following command will create a Cloudformation Stack and deploy your SAM resources.

```bash
sam deploy \
    --template-file packaged.yaml \
    --stack-name dynamicip \
    --capabilities CAPABILITY_IAM
```

Or run the command 

```bash
./deploy.sh REPLACE_THIS_WITH_YOUR_CLOUDFORMATION_STACK_NAME
```

> **See [Serverless Application Model (SAM) HOWTO Guide](https://github.com/awslabs/serverless-application-model/blob/master/HOWTO.md) for more details in how to get started.**


# Appendix

## AWS CLI commands

AWS CLI commands to package, deploy and describe outputs defined within the cloudformation stack:

```bash
sam package \
    --template-file template.yaml \
    --output-template-file packaged.yaml \
    --s3-bucket REPLACE_THIS_WITH_YOUR_S3_BUCKET_NAME

sam deploy \
    --template-file packaged.yaml \
    --stack-name dynamicip \
    --capabilities CAPABILITY_IAM \
    --parameter-overrides MyParameterSample=MySampleValue

aws cloudformation describe-stacks \
    --stack-name dynamicip --query 'Stacks[].Outputs'
```
