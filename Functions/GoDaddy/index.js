const aws = require('aws-sdk');
const axios = require('axios')
axios.defaults.headers.common['Authorization'] = 'sso-key '+process.env.GODADDY_API_KEY+':'+process.env.GODADDY_API_SECRET+'';
const domainRequestCache = {};

exports.handler = async (payload, context) => {
    if(!('domains' in payload)){
        console.error('missing domain array');
        return;
    }

    const aryPromises = [];
    for(const domain of payload.domains){
        aryPromises.push(
            parseDomain(domain)
                .then(getDomainData)
                .then(record => putUpdate(record, payload.ip))
                .catch((e) => console.error(domain+':', e))
        );
    }

    await Promise.all(aryPromises);
};

const parseDomain = async domain => {
    // split the domain into pieces
    const domainArray = domain.split('.');

    // determine the slice count to get the subdomain and main domain
    const slice = (domainArray.length - 2);

    // get the subdomain
    const arySlicedSubdomain = domainArray.slice(0, slice);

    // get the main and TLD
    const arySlicedDomain = domainArray.slice(slice);

    // return the domain pieces
    return {
        subDomain: arySlicedSubdomain.join('.'),
        domain: arySlicedDomain.join('.')
    };
};


const getDomainData = async ({subDomain, domain}) => {
    let response;
    if(domain in Object.keys(domainRequestCache)){
        // retrieve the cache for this run
        response =  domainRequestCache[domain];
    }else{
        // get the domain data if there is no cached item
        const url = 'https://api.godaddy.com/v1/domains/'+domain+'/records';
        const result = await axios.get( url );
        response = domainRequestCache[domain] = result.data;
    }

    // for each of the records, attempt to match what godaddy has returned.
    for(const record of response){
        if(subDomain === ''){
            // if the subdomain is empty, then this is a top level domain
            // let's match where the name is an '@' symbol as that's godaddy's root element symbol
            // and we only want to match on CNAME or A records
            if(record.name === '@' && (record.type === 'CNAME' || record.type === 'A')){
                return {record, subDomain, domain};
            }
        }else{
            // otherwise match on the exact sibdomain
            if(record.name === subDomain){
                return {record, subDomain, domain};
            }
        }
    }
};

const putUpdate = async (objRecordToUpdate, data) => {
    // if we don't have an objRecordToUpdate, we wern't able to find the record.
    // fail gracefully with the catch above.
    if(!objRecordToUpdate){
        throw new Error('unable to find requested domain')
    }

    // call the update to the godaddy api, only changing the IP address
    const url = 'https://api.godaddy.com/v1/domains/'+objRecordToUpdate.domain+'/records/'+objRecordToUpdate.record.type+'/'+objRecordToUpdate.subDomain+'';

    // return the promise.
    return axios.put( url, [ { data } ] );
};

