const aws = require('aws-sdk');
const route53 = new aws.Route53();


exports.handler = async (payload, context) => {
    // add the period after each host to match later
    payload.domains = payload.domains.map((host) => {return host+'.';});
    console.log('attempting to update the following domains', payload.domains);

    // set some variables
    let upserts = {};
    let data;

    // get all of the zones
    try{
        data = await route53.listHostedZones({}).promise();
    }catch (e) {
        console.error('listHostedZones error');
        console.error(e);
        return;
    }

    for(const zone of data.HostedZones){
        let recordSetdata;

        // get all of the resources for each zone
        try{
            recordSetdata = await route53.listResourceRecordSets({
                HostedZoneId: zone.Id,
                MaxItems:'100'
            }).promise();
        }catch (e) {
            console.error('listResourceRecordSets error');
            console.error(e);
            continue;
        }

        for(const resource of recordSetdata.ResourceRecordSets){
            if(resource.Type === "A" && payload.domains.indexOf(resource.Name) >= 0){
                // found a match, let's see if it needs updated
                if(resource.ResourceRecords[0].Value !== payload.ip){
                    if(!(zone.Id in upserts)){
                        upserts[zone.Id] = [];
                    }
                    // let's schedule it to be updated
                    upserts[zone.Id].push({
                        Action: "UPSERT",
                        ResourceRecordSet: {
                            Name: resource.Name,
                            ResourceRecords: [
                                {
                                    Value: payload.ip
                                }
                            ],
                            TTL: 60,
                            Type: "A"
                        }
                    });
                }
            }
        }
    }

    let returnVal = "noting updated";
    if(Object.keys(upserts).length > 0){
        let promises = [];
        for(const [zoneId, upsertStatement] of Object.entries(upserts)){
            promises.push(
                updateZone(zoneId, upsertStatement)
            );
        }
        returnVal = JSON.stringify(
            await Promise.all(promises)
        );
    }

    console.log(returnVal);
    return returnVal;
};

const updateZone = async (zoneId, upserts) => {
    let response;
    try{
        response = await route53.changeResourceRecordSets({
            ChangeBatch: {
                Changes: upserts,
                Comment: "arlington server ip address update"
            },
            HostedZoneId: zoneId
        }).promise();
    }catch(e){
        console.error('changeResourceRecordSets error');
        console.error(e);
        return
    }
    return response;
};